﻿using Caliburn.Micro;
using mvvm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace mvvm.ViewModels
{
    public class LoadLogInViewModel: Conductor<Object>
    {
		//private QuizViewModel QuizViewModel;


		//private string _groupVisibility;

		//public string GroupVisibility
		//{
		//	get { return _groupVisibility; }
		//	set { _groupVisibility = value; NotifyOfPropertyChange(() => GroupVisibility); }
		//}

		private string _userID;

		public string UserID
		{
			get
			{
				return _userID;
			}
			set
			{
				_userID = value;
				NotifyOfPropertyChange(() => UserID);
			}
		}

		private string _userPassword;

		public string UserPassword
		{
			get
			{
				return _userPassword;
			}
			set
			{
				_userPassword = value;
				NotifyOfPropertyChange(() => UserPassword);
			}
		}


		public void CheckUserPassword(string userID, string userPassword)
		{
			if (String.IsNullOrWhiteSpace(SelectedDifficulty))
			{
				MessageBox.Show("Please choose level of difficulty");
			}
			else
			{
				XmlDocument xmlDocument = generateXML();
				if (checkUserAndPassword(xmlDocument) == true)
				{
					MessageBox.Show("You have entered correct details");
					LoadQuizView();
				}
				else
				{
					MessageBox.Show("User or password is incorrect!");
				}
			}

		}

		public Boolean checkUserAndPassword(XmlDocument xmlDoc)
		{
			Boolean realAccount = false;

			List<String> passList = loadPasswords(xmlDoc);
			List<String> userList = loadUsers(xmlDoc);

			String user = _userID;
			String password = _userPassword;

			for (int i = 0; i < passList.Count; i++)
			{
				if (passList[i] == password && userList[i] == user)
				{
					return true;
				}
			}

			return realAccount;
		}

		public static XmlDocument generateXML()
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load("UserAccounts.xml");

			return xmlDoc;
		}

		public List<String> loadPasswords(XmlDocument xmlDoc)
		{
			List<String> passList = new List<string>();
			String temp = "";
			int userCount = (int)countUserIds();
			int index = 1;

			while (index < userCount + 1)
			{
				XmlNode tempUserIDNode = xmlDoc.SelectSingleNode("//accounts/account/userpassword[@numberpass='" + index + "']");
				temp = tempUserIDNode.InnerText;

				passList.Add(temp);
				index++;
			}

			return passList;
		}

		public List<String> loadUsers(XmlDocument xmlDoc)
		{
			List<String> userList = new List<string>();
			String temp = "";
			int userCount = (int)countUserIds();
			int index = 1;


			while (index < userCount + 1)
			{
				XmlNode tempUserIDNode = xmlDoc.SelectSingleNode("//accounts/account/userid[@number='" + index + "']");
				temp = tempUserIDNode.InnerText;

				userList.Add(temp);
				index++;
			}

			return userList;
		}

		public static double countUserIds()
		{
			double userCount;
			var xDoc = XDocument.Load("UserAccounts.xml");
			userCount = (double)xDoc.XPathEvaluate("count(//accounts/account/userid)");

			return userCount;
		}


		private BindableCollection<String> _difficulty = new BindableCollection<string>();

		public BindableCollection<String> Difficulty
		{
			get { return _difficulty; }
			set { _difficulty = value; }
		}

		private string _selectedDifficulty;

		public string SelectedDifficulty
		{
			get { return _selectedDifficulty; }
			set { _selectedDifficulty = value; NotifyOfPropertyChange(() => SelectedDifficulty); }
		}


		public LoadLogInViewModel()
		{
			Difficulty.Add("easy");
			Difficulty.Add("medium");
			Difficulty.Add("hard");

		}

		public void LoadQuizView()
		{


			//GroupVisibility = "Hidden";
			ActivateItem(new QuizViewModel(SelectedDifficulty));

			//QuizViewModel = new QuizViewModel(SelectedDifficulty);
			//QuizView quizView = new QuizView();
			//quizView.DataContext = QuizViewModel;
			//quizView.Show();
			//this.TryClose();
		}


		public bool CanCheckUserPassword(string userID, string userPassword)
		{
			if (String.IsNullOrWhiteSpace(userID) || String.IsNullOrWhiteSpace(userPassword))
			{
				return false;
			}
			else
			{
				return true;
			}
		}



	}
}
