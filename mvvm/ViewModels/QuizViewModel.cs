﻿using Caliburn.Micro;
using mvvm.Models;
using mvvm.Models.Commands;
using mvvm.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace mvvm.ViewModels
{
	public class QuizViewModel : Conductor<object>
	{
		private string _questionText;
		private RadioButton[] _questionOptions;
		private CheckBox[] _questionCheckBoxOptions;
		private string _difficulty;
		private int _correctOption;
		private int _questionNumber;
		private BindableCollection<QuestionModel> _questionModels = new BindableCollection<QuestionModel>();
		private int _selectedOption ;
		private string _questionType;

		//private ResultsViewModel resultsViewModel;

		public int correctOption;
		public List<int> correctCheckBoxes = new List<int>();
		public double score = 0;
		public double grade;

		public int viewCounter = 0;
		XmlDocument XmlDoc = generateXML();
		public Command nextQuestionCommand { get; private set; }
		public Command indexExchange { get; private set; }
		public static string setDifficulty;


		private List<int> _correctCheckBoxes;

		public List<int> CorrectCheckBoxes
		{
			get { return _correctCheckBoxes; }
			set { _correctCheckBoxes = value; NotifyOfPropertyChange(() => CorrectOption); }
		}



		public string QuestionText
		{
			get { return _questionText; }
			set { _questionText = value; NotifyOfPropertyChange(() => QuestionText); }
		}
		public RadioButton[] QuestionOptions
		{
			get { return _questionOptions; }
			set { _questionOptions = value; NotifyOfPropertyChange(() => QuestionOptions); }
		}
		public string Difficulty
		{
			get { return _difficulty; }
			set { _difficulty = value; NotifyOfPropertyChange(() => Difficulty); }
		}
		public int CorrectOption
		{
			get { return _correctOption; }
			set { _correctOption = value; NotifyOfPropertyChange(() => CorrectOption); }
		}
		public int QuestionNumber
		{
			get { return _questionNumber; }
			set { _questionNumber = value; }
		}
		public BindableCollection<QuestionModel> QuestionModels
		{
			get { return _questionModels; }
			set { _questionModels = value; NotifyOfPropertyChange(() => QuestionModels); }
		}
		public int SelectedOption
		{
			get { return _selectedOption; }
			set { _selectedOption = value; NotifyOfPropertyChange(() => SelectedOption); }
		}
		public CheckBox[] QuestionCheckBoxOptions
		{
			get { return _questionCheckBoxOptions; }
			set { _questionCheckBoxOptions = value; NotifyOfPropertyChange(() => QuestionCheckBoxOptions); }
		}
		public string QuestionTypeFullProperty
		{
			get { return _questionType; }
			set { _questionType = value; NotifyOfPropertyChange(() => QuestionTypeFullProperty); }
		}


		public BindableCollection<QuestionModel> SelectedQuestions = new BindableCollection<QuestionModel>();



		private string _visibleRadio;

		public string  VisibleRadio
		{
			get { return _visibleRadio; }
			set { _visibleRadio = value; NotifyOfPropertyChange(() => VisibleRadio);}
		}

		private string _visibleCheck;

		public string VisibleCheck
		{
			get { return _visibleCheck; }
			set { _visibleCheck = value; NotifyOfPropertyChange(() => VisibleCheck);}
		}


		



		public void Check()
		{

			
			if(SelectedQuestions.ElementAt(viewCounter).QuestionType == "radio")
			{
				for (int i = 0; i < SelectedQuestions.ElementAt(viewCounter).QuestionOptions.Length; i++)
				{
					if (SelectedQuestions.ElementAt(viewCounter).QuestionOptions[i].IsChecked == true)
					{
						if (i + 1 == SelectedQuestions.ElementAt(viewCounter).CorrectOption)
						{
							score++;
						}

					}
				}
			}
			else
			{
				int countCorrect = 0;
												
				for (int i = 0; i < SelectedQuestions.ElementAt(viewCounter).QuestionCheckBoxOptions.Length; i++)
				{
					if (SelectedQuestions.ElementAt(viewCounter).QuestionCheckBoxOptions[i].IsChecked == true)
					{
						
						for(int k=0; k< SelectedQuestions.ElementAt(viewCounter).CorrectCheckBoxes.Count(); k++)
						{
							
							if(SelectedQuestions.ElementAt(viewCounter).CorrectCheckBoxes.ElementAt(k).Equals(i + 1))
							{
								
								countCorrect++;
							}
						}
												
					}
					
				}

				

				if(countCorrect == SelectedQuestions.ElementAt(viewCounter).CorrectCheckBoxes.Count())
				{
					score++;
				}
			}

			

			MessageBox.Show("Your score is " + score.ToString());

			viewCounter++;
			
			if (viewCounter < 5)
			{
				
				displayQuestions(SelectedQuestions, viewCounter);

			}
			else
			{
				grade = 2 * score;
				MessageBox.Show("End of Quiz!");
				ActivateItem(new ResultsViewModel(grade.ToString()));


				//resultsViewModel = new ResultsViewModel();
				
				//resultsViewModel.Results = grade.ToString();
				//ResultsView resultsView = new ResultsView();
				//resultsView.DataContext = resultsViewModel;
				//resultsView.Show();
				//this.TryClose();

			}

		}
		
		public QuizViewModel(string SelectedDifficulty)
		{
			setDifficulty = SelectedDifficulty;
			QuestionModels = generateQuestionObjects(XmlDoc);
			SelectedQuestions = getFiveQuestions(QuestionModels);
			displayQuestions(SelectedQuestions, viewCounter);
			nextQuestionCommand = new Command(Check);
			
			//indexExchange = new Command(listViewItemToRadioButton);
			


		}

		public void listViewItemToRadioButton(BindableCollection<QuestionModel>  SelectedQuestions, int listIndex)
		{
			
				listIndex = SelectedOption;
				//MessageBox.Show(listIndex.ToString());
				if (SelectedQuestions.ElementAt(viewCounter).QuestionType == "radio")
				{
					SelectedQuestions.ElementAt(viewCounter).QuestionOptions[listIndex].IsChecked = true;
					//MessageBox.Show("Cap de pod");
				}
				else
				{
					SelectedQuestions.ElementAt(viewCounter).QuestionCheckBoxOptions[listIndex].IsChecked = true;
					//MessageBox.Show("Cap de pod");
				}
			
			
								
		}

		public void displayQuestions(BindableCollection<QuestionModel> questionModelsObjects, int viewCounter)
		{
			if(questionModelsObjects.ElementAt(viewCounter).QuestionType == "check")
			{
				VisibleCheck = "Visible";
				VisibleRadio = "Hidden";
				QuestionText = questionModelsObjects.ElementAt(viewCounter).QuestionText;
				QuestionCheckBoxOptions = questionModelsObjects.ElementAt(viewCounter).QuestionCheckBoxOptions;
				Difficulty = questionModelsObjects.ElementAt(viewCounter).Difficulty;
				CorrectCheckBoxes = questionModelsObjects.ElementAt(viewCounter).CorrectCheckBoxes; ;
			}
			else
			{
				VisibleCheck = "Hidden";
				VisibleRadio = "Visible";
				QuestionText = questionModelsObjects.ElementAt(viewCounter).QuestionText;
				QuestionOptions = questionModelsObjects.ElementAt(viewCounter).QuestionOptions;
				Difficulty = questionModelsObjects.ElementAt(viewCounter).Difficulty;
				CorrectOption = questionModelsObjects.ElementAt(viewCounter).CorrectOption; ;
			}
			
		}

		public static BindableCollection<QuestionModel> getFiveQuestions(BindableCollection<QuestionModel> _questionModelsList)
		{
			List<int> randomList = randomListGenerator(_questionModelsList);
			BindableCollection<QuestionModel> fiveQuestionsList = new BindableCollection<QuestionModel>();


			foreach (int q in randomList)
			{
				fiveQuestionsList.Add(_questionModelsList.ElementAt(q));
			}


			return fiveQuestionsList;
		}

		public static XmlDocument generateXML()
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load("QuizQuestions.xml");

			return xmlDoc;
		}

		public BindableCollection<QuestionModel> generateQuestionObjects(XmlDocument xmlDocument)
		{
			BindableCollection<QuestionModel> QuizQuestions = new BindableCollection<QuestionModel>();
			string questionText;
			string difficulty;
			

			//RadioButton[] optionsRadioButtons;
			xmlDocument = generateXML();

			int counter = 0;

			double countQ = countQuestions();

			while (counter < countQ)
			{
				int questionNumber = counter + 1;

				questionText = generateQuestion(xmlDocument, questionNumber);

				difficulty = getDifficulty(xmlDocument, questionNumber);

				if (QuestionTypeFullProperty == "radio")
				{
					QuizQuestions.Add(new QuestionModel
					{
						QuestionText = questionText,
						QuestionType = QuestionTypeFullProperty,
						QuestionOptions = generateOptionRadioButtons(xmlDocument, questionNumber),
						Difficulty = difficulty,
						QuestionNumber = questionNumber,
						CorrectOption = correctOption
					});
				}
				else
				{
					QuizQuestions.Add(new QuestionModel
					{
						QuestionText = questionText,
						QuestionType = QuestionTypeFullProperty,
						QuestionCheckBoxOptions = generateOptionCheckBoxes(xmlDocument, questionNumber),
						Difficulty = difficulty,
						QuestionNumber = questionNumber,
						CorrectCheckBoxes = getCorrectCheckBoxes(xmlDocument, questionNumber)
					});
					
				}

				counter++;
			}
				return QuizQuestions;
		}

		public RadioButton[] generateOptionRadioButtons(XmlDocument xmlDoc, int questNo)
		{
			int optionsNo = (int)countQuestionOptions(questNo);
			RadioButton[] optionsRadioButtons = new RadioButton[optionsNo];
			int optNo = 1;

			for (int pos = 0; pos < optionsNo; pos++)
			{
				XmlNode tempOptionNode = xmlDoc.SelectSingleNode("//quizz/question[@number='" + questNo + "']/option[@number='" + optNo + "']");
				optionsRadioButtons[pos] = new RadioButton();
				optionsRadioButtons[pos].Content = tempOptionNode.InnerText;
				optionsRadioButtons[pos].GroupName = "OptionsGroup";

				if (tempOptionNode.Attributes["correct"].Value == "true")
				{
					correctOption = optNo;

				}

				optNo++;
			}

			return optionsRadioButtons;
		}

		public CheckBox[] generateOptionCheckBoxes(XmlDocument xmlDoc, int questNo)
		{
			
			int optionsNo = (int)countQuestionOptions(questNo);
			CheckBox[] optionsCheckBoxes = new CheckBox[optionsNo];
			
			int optNo = 1;

			for (int pos = 0; pos < optionsNo; pos++)
			{
				XmlNode tempOptionNode = xmlDoc.SelectSingleNode("//quizz/question[@number='" + questNo + "']/option[@number='" + optNo + "']");
				optionsCheckBoxes[pos] = new CheckBox();
				optionsCheckBoxes[pos].Content = tempOptionNode.InnerText;


				//if (tempOptionNode.Attributes["correct"].Value == "true")
				//{
				//	correctCheckBoxes.Add(optNo);

				//}

				

				optNo++;
			}

			return optionsCheckBoxes;
		}

		public List<int> getCorrectCheckBoxes(XmlDocument xmlDoc, int questNo)
		{
			List<int> tempAnswers = new List<int>();
			int optionsNo = (int)countQuestionOptions(questNo);
			int optNo = 1;

			for (int pos = 0; pos < optionsNo; pos++)
			{
				XmlNode tempOptionNode = xmlDoc.SelectSingleNode("//quizz/question[@number='" + questNo + "']/option[@number='" + optNo + "']");
				
				if (tempOptionNode.Attributes["correct"].Value == "true")
				{
					tempAnswers.Add(optNo);

				}



				optNo++;
			}


			return tempAnswers;
		}

		public string generateQuestion(XmlDocument xmlDoc, int questNo)
		{
			string temp;
			XmlNode titleNode = xmlDoc.SelectSingleNode("//quizz/question[@number='" + questNo + "']/text");
			temp = titleNode.InnerText;
			QuestionTypeFullProperty = xmlDoc.SelectSingleNode("//quizz/question[@number='" + questNo + "']").Attributes["type"].InnerText;

			return temp;
		}


		public string getDifficulty(XmlDocument xmlDoc, int questNo)
		{
			string difficulty;
			XmlNode titleNode = xmlDoc.SelectSingleNode("//quizz/question[@number='" + questNo + "']");
			difficulty = titleNode.Attributes["difficulty"].InnerText;
			return difficulty;
		}

		public static double countQuestionOptions(int questNo)
		{

			var xDoc = XDocument.Load("QuizQuestions.xml");
			double number = (double)xDoc.XPathEvaluate("count(//quizz/question[@number='" + questNo + "']/option)");

			return number;
		}

		public double countQuestions()
		{
			var xDoc = XDocument.Load("QuizQuestions.xml");
			double questionCount = (double)xDoc.XPathEvaluate("count(//quizz/question)");

			return questionCount;
		}

		public static List<int> randomListGenerator(BindableCollection<QuestionModel> _questionModels)
		{
			List<int> randomList = new List<int>();
			Random generator = new Random();
			int myNumber = generator.Next(1, 30);
			int h = 0;

			while (h < 5)
			{
				if (!randomList.Contains(myNumber))
				{
					if (_questionModels.ElementAt(myNumber).Difficulty.Equals(setDifficulty))
					{
						randomList.Add(myNumber);
						h++;
					}

				}
				myNumber = generator.Next(1, 30);


			}

			return randomList;
		}

		
		public double trackScore(RadioButton[] radioButtons)
		{
			for (int s = 0; s < radioButtons.Length; s++)
			{
				if (radioButtons[s].IsChecked == true)
				{

					if (s + 1 == correctOption)
					{
						score++;
					}


				}
			}

			return score;
		}


	}
}
