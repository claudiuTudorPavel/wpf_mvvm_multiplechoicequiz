﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace mvvm.Models
{
	public class QuestionModel
	{
		public string QuestionText { get; set; }
		public string QuestionType { get; set; }
		public RadioButton[] QuestionOptions { get; set; }
		public CheckBox[] QuestionCheckBoxOptions { get; set; }
		public string Difficulty { get; set; }
		public int CorrectOption { get; set; }
		public List<int> CorrectCheckBoxes { get; set; }
		public int QuestionNumber { get; set; }
		




	}
}
