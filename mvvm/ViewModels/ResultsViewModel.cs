﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvvm.ViewModels
{
    public class ResultsViewModel: Screen
    {

		private string _results;

		public string Results
		{
			get { return _results; }
			set { _results = value; }
		}

		public ResultsViewModel(string passedResults)
		{
			Results = passedResults;
		}

	}
}
